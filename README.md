# Large Document Sample (DITA-OT User Guide)

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DITA-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the exe files.*

*For example `DeltaXML-DITA-Compare-8_0_0_n/samples/sample-name`.*

---

## Summary

This sample compares two versions of the DITA Open Toolkit's User Guide, doc-1-6-3/userguide.ditamap and doc-1-7-2/userguide.ditamap, using the 'topic set', 'map pair' and 'unified map' map result structures. This is intended to illustrates the results of the comparison on a reasonable sized document. For concept details see: [DITA-OT User Guide](https://docs.deltaxml.com/dita-compare/latest/large-document-sample-dita-ot-user-guide-2135657.html).

## Script Features

The output of the 'topic set', 'map pair' and 'unified map' comparisons are put into the topic-set-A, map-pair-B and unified-map-B directories respectively. These directories contain a result-alias.ditamap file whose content references the actual results, but can be used as a result in its own right. Note that in the case of the map pair result, the alias combines the two main result maps into a single document.

## Using a Batch File

Run the rundemo.bat batch file either by entering rundemo from the command-line or by double-clicking on this file from Windows Explorer. This script runs the same comparison twice with different output settings (topic-set and unified-map).

## Running the sample from the Command line

The 'topic-set' sample comparison can also be run from the command line, using the following:

    ..\..\bin\deltaxml-dita.exe compare map doc-1-6-3/userguide.ditamap doc-1-7-2/userguide.ditamap topic-set-A map-result-origin=A map-result-structure=topic-set

The 'map pair' or 'unified-map' comparison can be run in a similar manner to the 'topic set', but in this case the map-result-orign is B, the map-result-structure is map-pair or unified-map, and the map-copy-location is map-pair-B or unified-map-B.